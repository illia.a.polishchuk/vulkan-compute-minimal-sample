#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vulkan/vulkan.h>

/*
#version 430 core

layout(std430, binding = 0) buffer g_shader_in_data
{
    float inData;
};

layout(std430, binding = 1) buffer g_shader_out_data
{
    float outData;
};

void main()
{
    outData = inData * inData;
}
*/
unsigned char shader_compute_spv[784] = {
    0x03, 0x02, 0x23, 0x07, 0x00, 0x00, 0x01, 0x00, 0x0a, 0x00, 0x0d, 0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x11, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x47, 0x4c,
    0x53, 0x4c, 0x2e, 0x73, 0x74, 0x64, 0x2e, 0x34, 0x35, 0x30, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x03, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x05, 0x00, 0x05, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
    0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x06, 0x00, 0x04, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00,
    0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x02, 0x00,
    0x00, 0x00, 0xae, 0x01, 0x00, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x47, 0x4c, 0x5f, 0x47, 0x4f, 0x4f, 0x47, 0x4c, 0x45,
    0x5f, 0x63, 0x70, 0x70, 0x5f, 0x73, 0x74, 0x79, 0x6c, 0x65, 0x5f, 0x6c, 0x69, 0x6e, 0x65, 0x5f, 0x64, 0x69, 0x72,
    0x65, 0x63, 0x74, 0x69, 0x76, 0x65, 0x00, 0x00, 0x04, 0x00, 0x08, 0x00, 0x47, 0x4c, 0x5f, 0x47, 0x4f, 0x4f, 0x47,
    0x4c, 0x45, 0x5f, 0x69, 0x6e, 0x63, 0x6c, 0x75, 0x64, 0x65, 0x5f, 0x64, 0x69, 0x72, 0x65, 0x63, 0x74, 0x69, 0x76,
    0x65, 0x00, 0x05, 0x00, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00, 0x6d, 0x61, 0x69, 0x6e, 0x00, 0x00, 0x00, 0x00, 0x05,
    0x00, 0x07, 0x00, 0x07, 0x00, 0x00, 0x00, 0x67, 0x5f, 0x73, 0x68, 0x61, 0x64, 0x65, 0x72, 0x5f, 0x6f, 0x75, 0x74,
    0x5f, 0x64, 0x61, 0x74, 0x61, 0x00, 0x00, 0x00, 0x06, 0x00, 0x05, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x6f, 0x75, 0x74, 0x44, 0x61, 0x74, 0x61, 0x00, 0x05, 0x00, 0x03, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x05, 0x00, 0x07, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x67, 0x5f, 0x73, 0x68, 0x61, 0x64, 0x65, 0x72, 0x5f,
    0x69, 0x6e, 0x5f, 0x64, 0x61, 0x74, 0x61, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x05, 0x00, 0x0c, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x69, 0x6e, 0x44, 0x61, 0x74, 0x61, 0x00, 0x00, 0x05, 0x00, 0x03, 0x00, 0x0e, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x03, 0x00, 0x07, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x47,
    0x00, 0x04, 0x00, 0x09, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00,
    0x09, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x48, 0x00, 0x05, 0x00, 0x0c, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47, 0x00, 0x03, 0x00, 0x0c, 0x00,
    0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x47, 0x00, 0x04, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x13, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x21, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00,
    0x00, 0x16, 0x00, 0x03, 0x00, 0x06, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x03, 0x00, 0x07, 0x00,
    0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x07,
    0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
    0x15, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x04,
    0x00, 0x0a, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x03, 0x00, 0x0c, 0x00,
    0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x20, 0x00, 0x04, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x0c,
    0x00, 0x00, 0x00, 0x3b, 0x00, 0x04, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
    0x20, 0x00, 0x04, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x36, 0x00, 0x05,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xf8, 0x00,
    0x02, 0x00, 0x05, 0x00, 0x00, 0x00, 0x41, 0x00, 0x05, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x0e,
    0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x3d, 0x00, 0x04, 0x00, 0x06, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00,
    0x10, 0x00, 0x00, 0x00, 0x41, 0x00, 0x05, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00,
    0x00, 0x0b, 0x00, 0x00, 0x00, 0x3d, 0x00, 0x04, 0x00, 0x06, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x12, 0x00,
    0x00, 0x00, 0x85, 0x00, 0x05, 0x00, 0x06, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x13,
    0x00, 0x00, 0x00, 0x41, 0x00, 0x05, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x15, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00,
    0x0b, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x03, 0x00, 0x15, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0xfd, 0x00, 0x01,
    0x00, 0x38, 0x00, 0x01, 0x00};

#define ERROR_CHECK(ERROR_CODE, ERROR_STRING)                                                                          \
    if (ERROR_CODE)                                                                                                    \
    {                                                                                                                  \
        fprintf(stderr, "ERROR: line: %d ; code: %d ; message: %s\n", __LINE__, ERROR_CODE, ERROR_STRING);             \
        abort();                                                                                                       \
    }

int main()
{
    VkApplicationInfo appInfo = {0};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "vulkan-compute-minimal-sample";
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo instanceCreateInfo = {0};
    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pApplicationInfo = &appInfo;

#ifdef __APPLE__ // Enable extensions for MoltenVK support
    const char *portabilityExtension = VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME;
    instanceCreateInfo.ppEnabledExtensionNames = &portabilityExtension;
    instanceCreateInfo.enabledExtensionCount = 1;
    instanceCreateInfo.flags |= VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR;
#endif

    VkInstance instance = VK_NULL_HANDLE;
    VkResult result = vkCreateInstance(&instanceCreateInfo, NULL, &instance);
    ERROR_CHECK(result, "vkCreateInstance failed");

    uint32_t physicalDeviceCount = 0;
    result = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, NULL);
    ERROR_CHECK(result, "vkEnumeratePhysicalDevices failed");
    ERROR_CHECK(!physicalDeviceCount, "vkEnumeratePhysicalDevices physical devices not found");

    VkPhysicalDevice *physicalDevices = malloc(sizeof(VkPhysicalDevice) * physicalDeviceCount);
    ERROR_CHECK(!physicalDevices, "VkPhysicalDevice malloc failed");

    result = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDevices);
    ERROR_CHECK(result, "vkEnumeratePhysicalDevices failed");

    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

    // 1 priority -- Intel
    for (uint32_t i = 0; i < physicalDeviceCount; ++i)
    {
        VkPhysicalDeviceProperties deviceProperties = {0};
        vkGetPhysicalDeviceProperties(physicalDevices[i], &deviceProperties);

        char deviceNameLower[VK_MAX_PHYSICAL_DEVICE_NAME_SIZE] = {0};
        for (size_t i = 0; deviceProperties.deviceName[i]; ++i)
            deviceNameLower[i] = tolower(deviceProperties.deviceName[i]);

        if (strstr(deviceNameLower, "intel") != NULL)
        {
            physicalDevice = physicalDevices[i];
            break;
        }
    }

    // 2 priority -- First in order
    if (!physicalDevice)
    {
        physicalDevice = physicalDevices[0];
    }

    VkPhysicalDeviceProperties deviceProperties = {0};
    vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

    printf("INFO: Selected physical device: %s\n\n", deviceProperties.deviceName);

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, NULL);

    VkQueueFamilyProperties *queueFamilies = malloc(sizeof(VkQueueFamilyProperties) * queueFamilyCount);
    ERROR_CHECK(!queueFamilies, "VkQueueFamilyProperties malloc failed");

    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies);

    uint32_t computeQueueFamilyIndex = -1;
    int computeQueueFamilyFound = VK_FALSE;
    for (uint32_t i = 0; i < queueFamilyCount; ++i)
    {
        if (queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
        {
            computeQueueFamilyIndex = i;
            computeQueueFamilyFound = VK_TRUE;
            break;
        }
    }

    ERROR_CHECK(!computeQueueFamilyFound, "vkGetPhysicalDeviceQueueFamilyProperties queue family with "
                                          "compute bit not found");

    VkDeviceQueueCreateInfo queueCreateInfo = {0};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = computeQueueFamilyIndex;
    queueCreateInfo.queueCount = 1;
    float queuePriority = 1.0f;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkPhysicalDeviceFeatures deviceFeatures = {0};

    VkDeviceCreateInfo deviceCreateInfo = {0};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
    deviceCreateInfo.queueCreateInfoCount = 1;
    deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

    VkDevice logicalDevice = VK_NULL_HANDLE;
    result = vkCreateDevice(physicalDevice, &deviceCreateInfo, NULL, &logicalDevice);
    ERROR_CHECK(result, "vkCreateDevice failed");

    VkQueue computeQueue = VK_NULL_HANDLE;
    vkGetDeviceQueue(logicalDevice, computeQueueFamilyIndex, 0, &computeQueue);
    ERROR_CHECK(!computeQueue, "vkGetDeviceQueue failed");

    VkBufferCreateInfo bufferCreateInfo = {0};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = sizeof(float);
    bufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkBuffer inBuffer = VK_NULL_HANDLE;
    result = vkCreateBuffer(logicalDevice, &bufferCreateInfo, NULL, &inBuffer);
    ERROR_CHECK(result, "vkCreateDevice failed");

    VkBuffer outBuffer = VK_NULL_HANDLE;
    result = vkCreateBuffer(logicalDevice, &bufferCreateInfo, NULL, &outBuffer);
    ERROR_CHECK(result, "vkCreateDevice failed");

    VkMemoryRequirements memRequirementsIn = {0};
    vkGetBufferMemoryRequirements(logicalDevice, inBuffer, &memRequirementsIn);

    VkMemoryRequirements memRequirementsOut = {0};
    vkGetBufferMemoryRequirements(logicalDevice, outBuffer, &memRequirementsOut);

    VkPhysicalDeviceMemoryProperties memProperties = {0};
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

    uint32_t memoryIndexIn = 0;
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
    {
        if ((memRequirementsIn.memoryTypeBits & (1 << i)) &&
            memProperties.memoryTypes[i].propertyFlags &
                (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
        {
            memoryIndexIn = i;
            break;
        }
    }

    uint32_t memoryIndexOut = 0;
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
    {
        if ((memRequirementsOut.memoryTypeBits & (1 << i)) &&
            memProperties.memoryTypes[i].propertyFlags &
                (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
        {
            memoryIndexOut = i;
            break;
        }
    }

    VkMemoryAllocateInfo allocInfoIn = {0};
    allocInfoIn.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfoIn.allocationSize = memRequirementsIn.size;
    allocInfoIn.memoryTypeIndex = memoryIndexIn;

    VkDeviceMemory inMemory = VK_NULL_HANDLE;
    result = vkAllocateMemory(logicalDevice, &allocInfoIn, NULL, &inMemory);
    ERROR_CHECK(result, "vkAllocateMemory failed");

    VkMemoryAllocateInfo allocInfoOut = {0};
    allocInfoOut.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfoOut.allocationSize = memRequirementsOut.size;
    allocInfoOut.memoryTypeIndex = memoryIndexOut;

    VkDeviceMemory outMemory = VK_NULL_HANDLE;
    result = vkAllocateMemory(logicalDevice, &allocInfoOut, NULL, &outMemory);
    ERROR_CHECK(result, "vkAllocateMemory failed");

    result = vkBindBufferMemory(logicalDevice, inBuffer, inMemory, 0);
    ERROR_CHECK(result, "vkBindBufferMemory failed");
    result = vkBindBufferMemory(logicalDevice, outBuffer, outMemory, 0);
    ERROR_CHECK(result, "vkBindBufferMemory failed");

    float *inBufferMapped = NULL;
    result = vkMapMemory(logicalDevice, inMemory, 0, bufferCreateInfo.size, 0, (void **)&inBufferMapped);
    ERROR_CHECK(result, "vkMapMemory failed");

    float *outBufferMapped = NULL;
    result = vkMapMemory(logicalDevice, outMemory, 0, bufferCreateInfo.size, 0, (void **)&outBufferMapped);
    ERROR_CHECK(result, "vkMapMemory failed");

    VkShaderModuleCreateInfo createInfo = {0};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = sizeof(shader_compute_spv) / sizeof(shader_compute_spv[0]);
    createInfo.pCode = (uint32_t *)shader_compute_spv;

    VkShaderModule shaderComputeModule = VK_NULL_HANDLE;
    result = vkCreateShaderModule(logicalDevice, &createInfo, NULL, &shaderComputeModule);
    ERROR_CHECK(result, "vkCreateShaderModule failed");

    VkDescriptorSetLayoutBinding inLayoutBinding = {0};
    inLayoutBinding.binding = 0;
    inLayoutBinding.descriptorCount = 1;
    inLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    inLayoutBinding.pImmutableSamplers = NULL;
    inLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

    VkDescriptorSetLayoutBinding outLayoutBinding = {0};
    outLayoutBinding.binding = 1;
    outLayoutBinding.descriptorCount = 1;
    outLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    outLayoutBinding.pImmutableSamplers = NULL;
    outLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

    VkDescriptorSetLayoutBinding bindings[2] = {inLayoutBinding, outLayoutBinding};

    VkDescriptorSetLayoutCreateInfo layoutInfo = {0};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
    layoutInfo.pBindings = bindings;

    VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
    result = vkCreateDescriptorSetLayout(logicalDevice, &layoutInfo, NULL, &descriptorSetLayout);
    ERROR_CHECK(result, "descriptorSetLayout failed");

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {0};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;

    VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
    result = vkCreatePipelineLayout(logicalDevice, &pipelineLayoutInfo, NULL, &pipelineLayout);
    ERROR_CHECK(result, "vkCreatePipelineLayout failed");

    VkPipelineShaderStageCreateInfo computeShaderStageInfo = {0};
    computeShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    computeShaderStageInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
    computeShaderStageInfo.module = shaderComputeModule;
    computeShaderStageInfo.pName = "main";

    VkComputePipelineCreateInfo computePipelineCreateInfo = {0};
    computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    computePipelineCreateInfo.layout = pipelineLayout;
    computePipelineCreateInfo.stage = computeShaderStageInfo;

    VkPipeline computePipeline = VK_NULL_HANDLE;
    result =
        vkCreateComputePipelines(logicalDevice, VK_NULL_HANDLE, 1, &computePipelineCreateInfo, NULL, &computePipeline);
    ERROR_CHECK(result, "vkCreateComputePipelines failed");

    VkDescriptorPoolSize poolSizes[2] = {0};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    poolSizes[0].descriptorCount = 1;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    poolSizes[1].descriptorCount = 1;

    VkDescriptorPoolCreateInfo poolInfo = {0};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = sizeof(poolSizes) / sizeof(poolSizes[0]);
    poolInfo.pPoolSizes = poolSizes;
    poolInfo.maxSets = 1;

    VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
    result = vkCreateDescriptorPool(logicalDevice, &poolInfo, NULL, &descriptorPool);
    ERROR_CHECK(result, "vkCreateDescriptorPool failed");

    VkDescriptorSetAllocateInfo descriptorSetAllocInfo = {0};
    descriptorSetAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocInfo.descriptorPool = descriptorPool;
    descriptorSetAllocInfo.descriptorSetCount = 1;
    descriptorSetAllocInfo.pSetLayouts = &descriptorSetLayout;

    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    result = vkAllocateDescriptorSets(logicalDevice, &descriptorSetAllocInfo, &descriptorSet);
    ERROR_CHECK(result, "vkAllocateDescriptorSets failed");

    VkDescriptorBufferInfo inBufferInfo = {0};
    inBufferInfo.buffer = inBuffer;
    inBufferInfo.offset = 0;
    inBufferInfo.range = bufferCreateInfo.size;

    VkDescriptorBufferInfo outBufferInfo = {0};
    outBufferInfo.buffer = outBuffer;
    outBufferInfo.offset = 0;
    outBufferInfo.range = bufferCreateInfo.size;

    VkWriteDescriptorSet descriptorWrites[2] = {0};
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = descriptorSet;
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &inBufferInfo;

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = descriptorSet;
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].dstArrayElement = 0;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pBufferInfo = &outBufferInfo;

    vkUpdateDescriptorSets(logicalDevice, sizeof(descriptorWrites) / sizeof(descriptorWrites[0]), descriptorWrites, 0,
                           NULL);

    VkCommandPoolCreateInfo commandPoolInfo = {0};
    commandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    commandPoolInfo.queueFamilyIndex = computeQueueFamilyIndex;

    VkCommandPool commandPool = VK_NULL_HANDLE;
    result = vkCreateCommandPool(logicalDevice, &commandPoolInfo, NULL, &commandPool);
    ERROR_CHECK(result, "vkCreateCommandPool failed");

    VkCommandBufferAllocateInfo commandBufferAllocInfo = {0};
    commandBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandBufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    commandBufferAllocInfo.commandPool = commandPool;
    commandBufferAllocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
    result = vkAllocateCommandBuffers(logicalDevice, &commandBufferAllocInfo, &commandBuffer);
    ERROR_CHECK(result, "vkAllocateCommandBuffers failed");

    VkCommandBufferBeginInfo commandBufferBeginInfo = {0};
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
    ERROR_CHECK(result, "vkBeginCommandBuffer failed");

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline);

    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descriptorSet, 0,
                            NULL);

    vkCmdDispatch(commandBuffer, 1, 1, 1);

    result = vkEndCommandBuffer(commandBuffer);
    ERROR_CHECK(result, "vkEndCommandBuffer failed");

    VkFenceCreateInfo fenceInfo = {0};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    VkFence fence = VK_NULL_HANDLE;
    result = vkCreateFence(logicalDevice, &fenceInfo, NULL, &fence);
    ERROR_CHECK(result, "vkCreateFence failed");

    result = vkResetFences(logicalDevice, 1, &fence);
    ERROR_CHECK(result, "vkResetFences failed");

    VkSubmitInfo submitInfo = {0};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    *inBufferMapped = 10;
    printf("Submitting operation 10*10 to GPU...\n");

    result = vkQueueSubmit(computeQueue, 1, &submitInfo, fence);
    ERROR_CHECK(result, "vkQueueSubmit failed");

    result = vkWaitForFences(logicalDevice, 1, &fence, VK_TRUE, UINT64_MAX);
    ERROR_CHECK(result, "vkQueueWaitIdle failed");

    result = vkQueueWaitIdle(computeQueue);
    ERROR_CHECK(result, "vkQueueWaitIdle failed");

    printf("RESULT: %f\n", *outBufferMapped);

    // cleanup
    if (fence)
        vkDestroyFence(logicalDevice, fence, NULL);

    if (commandBuffer)
        vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);

    if (commandPool)
        vkDestroyCommandPool(logicalDevice, commandPool, NULL);

    if (descriptorPool)
        vkDestroyDescriptorPool(logicalDevice, descriptorPool, NULL);

    if (computePipeline)
        vkDestroyPipeline(logicalDevice, computePipeline, NULL);

    if (pipelineLayout)
        vkDestroyPipelineLayout(logicalDevice, pipelineLayout, NULL);

    if (descriptorSetLayout)
        vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, NULL);

    if (shaderComputeModule)
        vkDestroyShaderModule(logicalDevice, shaderComputeModule, NULL);

    if (outMemory)
        vkFreeMemory(logicalDevice, outMemory, NULL);

    if (inMemory)
        vkFreeMemory(logicalDevice, inMemory, NULL);

    if (outBuffer)
        vkDestroyBuffer(logicalDevice, outBuffer, NULL);

    if (inBuffer)
        vkDestroyBuffer(logicalDevice, inBuffer, NULL);

    if (logicalDevice)
        vkDestroyDevice(logicalDevice, NULL);

    if (physicalDevices)
        free(physicalDevices);

    if (instance)
        vkDestroyInstance(instance, NULL);

    return 0;
}
